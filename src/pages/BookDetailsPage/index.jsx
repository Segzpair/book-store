import { useQuery } from '@apollo/client';
import React, { useContext, useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router';
import { Context } from '../../App';
import { ReactComponent as BackArrow } from '../../assets/arrow.svg';
import { ReactComponent as CartIcon } from '../../assets/cart.svg';
import BookStats from '../../components/BookStats';
import LoadingScreen from '../../components/LoadingScreen';
import { getBookQuery } from '../../graphql/queries';

import {
  addItemToCart,
  checkIsBookInStock,
  findBookById,
  getCopiesLeftSentence,
} from '../../utils';

import './BookDetailsPage.scss';

const BookDetailsPage = () => {
  const { bookId } = useParams();
  const history = useHistory();

  const [currentBook, setCurrentBook] = useState({});
  const [isAvailable, setIsAvailable] = useState(true);
  const {
    cartState,
    dispatchCart,
    testData,
    isTestData,
    setIsTestData,
  } = useContext(Context);

  // fetch data from API and update state
  const { loading: isLoading, error, data } = useQuery(getBookQuery(bookId));

  useEffect(() => {
    if (!isLoading && !error && data && data.book) {
      setCurrentBook(data.book);
    }
    // change to test data if error occurs or is already test data
    if ((!isLoading && error) || isTestData) {
      if (testData.length) {
        setCurrentBook(findBookById(testData, bookId));
      }
    }
  }, [isLoading, error, data, testData, bookId, isTestData]);

  useEffect(() => {
    if (!isLoading && !error && data && data.book) {
      setIsAvailable(checkIsBookInStock(cartState, currentBook));
    }
  }, [cartState, currentBook, isLoading, error, data]);

  const {
    authors,
    available_copies,
    full_description,
    genres,
    image_url,
    likes,
    number_of_purchases,
    price,
    publisher,
    rating,
    release_date,
    subtitle,
    tags,
    title,
  } = currentBook;

  return (
    <>
      {error && !isTestData ? (
        <div className="not-found">
          <h2>Oops! :(</h2>
          <p>An error occured while fetching data.</p>
          <button className="btn-primary" onClick={() => setIsTestData(true)}>
            Click Here to use Test Data
          </button>
        </div>
      ) : isLoading && !isTestData ? (
        <LoadingScreen />
      ) : (
        <div className="book-details-page">
          <div className="sidebar">
            <button className="back-btn" onClick={() => history.goBack()}>
              <BackArrow /> Back
            </button>
            <img
              src={image_url ? image_url : ''}
              alt={title}
              width="243"
              height="364.5"
            />
            <div className="purchase-details" data-wide>
              <p
                className={
                  available_copies ? 'quantity' : 'quantity out-of-stock'
                }>
                {getCopiesLeftSentence(available_copies)}
              </p>

              <p className="price">{price ? `$${price}` : 'N/A'}</p>

              <button
                className="add-to-cart"
                disabled={!isAvailable}
                hidden={available_copies <= 0}
                onClick={() => {
                  addItemToCart(dispatchCart, currentBook);
                }}>
                <CartIcon />
                Add to Cart
              </button>
            </div>
          </div>

          <button
            className="add-to-cart"
            disabled={!isAvailable}
            hidden={available_copies <= 0}
            onClick={() => {
              addItemToCart(dispatchCart, currentBook);
            }}
            data-mobile>
            <span className="icon">
              <CartIcon />
            </span>
            <span className="text">
              <span className="label">Add to Cart</span>
              <span
                className={
                  available_copies ? 'quantity' : 'quantity out-of-stock'
                }>
                {getCopiesLeftSentence(available_copies)}
              </span>
            </span>
            <span className="price">{price ? `$${price}` : 'N/A'}</span>
          </button>

          <div>
            <h1 className="title">{title ? title : 'No Book Selected'}</h1>

            <div>
              <p className="author">{authors ? authors[0].name : ''}</p>
              <p className="year">
                {release_date ? new Date(release_date).getFullYear() : 'N/A'}
              </p>
            </div>
            <hr />

            <div className="book-info">
              <BookStats {...{ likes, number_of_purchases, rating }} />

              <div className="genre">
                <h3>Genre</h3>
                <p>
                  {genres && genres.length > 0
                    ? genres.map((genre) => genre.name).join(', ')
                    : 'N/A'}
                </p>
              </div>

              <div className="tags">
                <h3>Tags</h3>
                <p>
                  {tags && tags.length > 0
                    ? tags.map((tags) => tags.name).join(', ')
                    : 'N/A'}
                </p>
              </div>

              <div className="publisher">
                <h3>Publisher</h3>
                <p>{publisher ? publisher : 'N/A'}</p>
              </div>

              <div className="released">
                <h3>Released</h3>
                <p>
                  {release_date
                    ? new Intl.DateTimeFormat('en', {
                        year: 'numeric',
                        month: 'long',
                        day: 'numeric',
                      }).format(new Date(release_date))
                    : 'N/A'}
                </p>
              </div>
            </div>
            <hr />

            <p className="subtitle">
              <strong>{subtitle}</strong>
            </p>

            <p
              className="description"
              dangerouslySetInnerHTML={{
                __html: full_description
                  ? full_description.replace(/\n/g, '<br />')
                  : '',
              }}
            />
          </div>
        </div>
      )}
    </>
  );
};

export default BookDetailsPage;
