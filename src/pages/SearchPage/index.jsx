import React, { useContext, useEffect, useState } from 'react';
import { Context } from '../../App';
import BookList from '../../components/BookList';
import { filterBySearchText } from '../../utils';

import './SearchPage.scss';

const SearchPage = ({ books }) => {
  const { searchText = '' } = useContext(Context);
  const [filteredBooks, setFilteredBooks] = useState(books);

  /* filter booklist using searchText  */
  useEffect(
    () =>
      setFilteredBooks(
        books.filter((book) => filterBySearchText(book, searchText))
      ),
    [books, searchText]
  );

  return (
    <div className="search-page">
      <BookList
        data={filteredBooks}
        listTitle={
          searchText
            ? `<h2>${
                filteredBooks.length > 0
                  ? filteredBooks.length > 1
                    ? filteredBooks.length + ' results'
                    : '1 result'
                  : 'No results'
              } found for "${searchText}".`
            : `<h2>Showing All Books</h2>`
        }
      />
    </div>
  );
};

export default SearchPage;
