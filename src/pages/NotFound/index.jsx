import React from 'react';
import { Link } from 'react-router-dom';

import './NotFound.scss';

const NotFound = () => {
  return (
    <div className="not-found">
      <h1>404 :(</h1>
      <h2>
        Ooops! <br /> Page Not Found!
      </h2>
      <p>You tried to visit</p>
      <p>
        <strong>{window.location.href}</strong>
      </p>

      <Link to="/">Click Here to Return Home</Link>
    </div>
  );
};

export default NotFound;
