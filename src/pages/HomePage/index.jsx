import React from 'react';
import BookList from '../../components/BookList';
import FeaturedBooksCarousel from '../../components/FeaturedBooksCarousel';
import { isBookFeatured } from '../../utils';

const HomePage = ({ booksData }) => {
  return (
    <>
      <FeaturedBooksCarousel books={booksData.filter(isBookFeatured)} />
      <BookList data={booksData} listTitle={`<h2>All Books</h2>`} />
    </>
  );
};

export default HomePage;
