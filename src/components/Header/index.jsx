import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import BrandLogo from '../../assets/brand-full.svg';
import BrandLite from '../../assets/brand-full.svg';
import { ReactComponent as Books } from '../../assets/books.svg';
import { ReactComponent as Cart } from '../../assets/cart.svg';
import { ReactComponent as SearchIcon } from '../../assets/search.svg';

import SearchBox from '../SearchBox';
import './Header.scss';

const Header = ({ cartState, setIsCartShown }) => {
  const [isSearchShown, setIsSearchShown] = useState(false);

  return (
    <header className="wrapper | page-header">
      <Link to="/">
        <picture>
          <source srcSet={`${BrandLogo} 960w`} media="(min-width: 960px)" />
          <source srcSet={`${BrandLite} 200w`} media="(min-width: 200px)" />
          <img src={BrandLite} alt="Book Store" width="360px" />
        </picture>
	  <span className="visually-hidden">Home</span>
      </Link>

      <SearchBox
        isSearchShown={isSearchShown}
        setIsSearchShown={setIsSearchShown}
      />

      <div className="header-links">
        <Link
          to="/search"
          className="search-button"
          onClick={() => {
            setIsSearchShown(true);
          }}
          data-mobile>
	  <span className="visually-hidden">Search</span>
          <SearchIcon />
        </Link>
        <Link to="/">
	  <span className="visually-hidden">All Books</span>
          <Books />
        </Link>
        <button className="cart-link" onClick={() => setIsCartShown(true)}>
	  <span className="visually-hidden">Show Cart</span>
          <Cart />
          <span
            className="cart-count"
            hidden={!(Array.isArray(cartState) && cartState.length > 0)}>
            {Array.isArray(cartState) && cartState.length > 0
              ? cartState.reduce(
                  (acc, currentValue) => acc + currentValue.quantityInCart,
                  0
                )
              : 0}
          </span>
        </button>
      </div>
    </header>
  );
};

export default Header;
