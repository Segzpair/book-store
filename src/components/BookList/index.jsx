import React from 'react';

import Book from '../Book';

import './BookList.scss';

const BookList = ({ data, listTitle }) => {
  // useContext for data

  return (
    <div className="book-list-container">
      <header dangerouslySetInnerHTML={{ __html: listTitle }} />
      <hr />
      <div className="book-list">
        {Array.isArray(data) && data.length ? (
          data.map((book) => <Book book={book} key={book.id} />)
        ) : (
          <p>No Books Available.</p>
        )}
      </div>
    </div>
  );
};

export default BookList;
