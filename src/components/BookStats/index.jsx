import React from 'react';
import { ReactComponent as PeopleIcon } from '../../assets/people.svg';
import { ReactComponent as HeartIcon } from '../../assets/heart.svg';
import StarsPreview from '../StarsPreview';

import './BookStats.scss';

const BookStats = ({ likes, number_of_purchases, rating }) => {
  return (
    <div className="book-stats">
      <div className="purchases">
        <PeopleIcon />
        {number_of_purchases}
      </div>
      <div className="likes">
        <HeartIcon /> {likes}
      </div>
      <div className="ratings">
        <span>Rating: {rating}</span>
        <div className="stars">
          <StarsPreview rating={rating} />
        </div>
      </div>
    </div>
  );
};

export default BookStats;
