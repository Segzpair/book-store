import React from 'react';

import { ReactComponent as BrandIcon } from '../../assets/brand-icon.svg';

import './LoadingScreen.scss';

const LoadingScreen = () => {
  return (
    <div className="loading-screen">
      <BrandIcon />
    </div>
  );
};

export default LoadingScreen;
