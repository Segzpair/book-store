import React from 'react';

import './Cart.scss';

import { ReactComponent as BackArrow } from '../../assets/arrow.svg';
import { ReactComponent as CartIcon } from '../../assets/cart.svg';
import CartItem from '../CartItem';
import { formatMoney } from '../../utils';
import Wrapper from '../../containers/Wrapper';

const Cart = ({ isCartShown, setIsCartShown, cartState, dispatchCart }) => {
  return (
    <Wrapper className={`cart${isCartShown ? ' cart--open' : ''}`}>
      <header className="cart__header">
        <button onClick={() => setIsCartShown(false)}>
          <BackArrow className="back-arrow" /> Back
        </button>

        <div>
          Your Cart
          <CartIcon className="cart-icon" />
        </div>
      </header>

      {cartState.length ? (
        cartState.map((cartItem) => (
          <CartItem cartItem={cartItem} key={cartItem.id} />
        ))
      ) : (
        <p>No Items in Your Cart</p>
      )}
      {cartState.length ? (
        <div className="summary">
          <p className="sub-total">
            <span className="sub-total-label">Subtotal</span>
            <span className="sub-total-amount">
              {cartState.length
                ? formatMoney(
                    cartState.reduce(
                      (acc, cur) => acc + cur.quantityInCart * cur.price,
                      0
                    )
                  )
                : formatMoney(0)}
            </span>
          </p>
          <button className="btn-checkout">
            <CartIcon />
            <span>Proceed to Checkout</span>
          </button>
        </div>
      ) : (
        ''
      )}
    </Wrapper>
  );
};

export default Cart;
