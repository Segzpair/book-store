import React, { useContext } from 'react';
import { Context } from '../../App';
import { formatMoney } from '../../utils';

import './CartItem.scss';

const CartItem = ({ cartItem }) => {
  const currentItem = { ...cartItem };
  const { title, image_url, authors, price, quantityInCart } = { ...cartItem };

  const { dispatchCart } = useContext(Context);

  return (
    <div className="cart-item">
      <img src={image_url} alt={title} width="110" height="163" />
      <div className="cart-item-text">
        <h3 className="title">{title}</h3>
        <p className="author">{authors[0].name}</p>
        <button
          className="btn-remove"
          onClick={() => {
            dispatchCart({ type: 'REMOVE_ITEM', payload: currentItem });
          }}>
          Remove
        </button>
      </div>
      <div className="price-details">
        <span className="unit-price">{price ? `$${price}` : 'N/A'}</span>

        <div className="quantity-controls">
          <button
            className="decrement"
            onClick={() => {
              dispatchCart({ type: 'DECREMENT_ITEM', payload: currentItem });
            }}>
	  <span className="visually-hidden">Decrement</span>
            -
          </button>
          <span className="quantity">{quantityInCart}</span>
          <button
            className="increment"
            onClick={() => {
              dispatchCart({
                type: 'INCREMENT_ITEM',
                payload: currentItem,
              });
            }}>
	  <span className="visually-hidden">Increment</span>
            +
          </button>
        </div>

        <span className="item-subtotal">
          {price ? `${formatMoney(price * quantityInCart)}` : '$0.00'}
        </span>
      </div>
    </div>
  );
};

export default CartItem;
