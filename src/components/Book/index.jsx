import React, { useContext, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { ReactComponent as CartIcon } from '../../assets/cart.svg';
import BookStats from '../BookStats';

import {
  checkIsBookInStock,
  getCopiesLeftSentence,
  addItemToCart,
} from '../../utils';

import './Book.scss';
import { Context } from '../../App';

const Book = ({ book }) => {
  const {
    authors,
    available_copies,
    genres,
    id,
    image_url,
    likes,
    number_of_purchases,
    price,
    rating,
    title,
    release_date,
  } = book;

  const { dispatchCart, cartState } = useContext(Context);
  const [isAvailable, setIsAvailable] = useState(
    checkIsBookInStock(cartState, book)
  );

  useEffect(() => {
    setIsAvailable(checkIsBookInStock(cartState, book));
  }, [cartState, book]);

  return (
    <div className="book">
      <img src={image_url} alt={title} width="110" height="183" />
      <div className="book-details">
        <Link to={`/books/${id}`}>
          <h3 className="book-title">{title}</h3>
        </Link>
        <p className="book-author-year">
          <span className="book-author">
            {authors.map((author) => author.name).join(', ')}
          </span>{' '}
          -{' '}
          <span className="book-year">
            {new Date(release_date).getFullYear()}
          </span>
        </p>
        <p className="book-genres">{genres.length ? genres[0].name : ''}</p>

        <BookStats {...{ likes, number_of_purchases, rating }} />

        <p className="purchase-details">
          <span className="book-price">${price}</span>
          <span
            className={`book-quantity${
              available_copies ? '' : ' out-of-stock'
            }`}>
            {getCopiesLeftSentence(available_copies)}
          </span>
        </p>
        <div className="margin-top:auto">
          <button
            disabled={!isAvailable}
            hidden={available_copies <= 0}
            onClick={() => {
              addItemToCart(dispatchCart, book);
            }}>
            <CartIcon />
            Add to Cart
          </button>
        </div>
      </div>
    </div>
  );
};

export default Book;
