import React from 'react';
import { Link } from 'react-router-dom';

import './FeaturedBookItem.scss';

const FeaturedBookItem = ({ book }) => {
  return (
    <Link
      to={`/books/${book.id}`}
      title={`${book.title} - ${book.authors[0].name}`}
      className="featured-book-item">
      <h3>{book.title}</h3>
      <img src={book.image_url} alt={book.title} width="220" height="330" />
    </Link>
  );
};

export default FeaturedBookItem;
