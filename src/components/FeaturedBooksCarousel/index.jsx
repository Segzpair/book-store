import React, { useCallback, useEffect, useState } from 'react';
import FeaturedBookItem from '../FeaturedBookItem';

import Slider from 'react-slick';

import './FeaturedBooksCarousel.scss';

const FeaturedBooksCarousel = ({ books }) => {
  const [slidesPerFrame, setSlidesPerFrame] = useState(2);
  /** updates the carousel */
  const updateVisibleCarouselItems = useCallback((event) => {
    let width = document.documentElement.clientWidth;

    let isSmall = width <= 480,
      canTakeTwo =
        (480 < width && width <= 530) || (530 < width && width < 880),
      canTakeThree =
        (530 < width && width <= 640) || (880 < width && width <= 1088),
      canTakeFour = 1088 < width && width <= 1320;

    switch (true) {
      case isSmall:
        setSlidesPerFrame(1);
        break;
      case canTakeTwo:
        setSlidesPerFrame(2);
        break;
      case canTakeThree:
        setSlidesPerFrame(3);
        break;
      case canTakeFour:
        setSlidesPerFrame(4);
        break;
      default:
        setSlidesPerFrame(5);
    }
  }, []);

  useEffect(() => {
    window.onload = updateVisibleCarouselItems;
  }, [updateVisibleCarouselItems]);

  return (
    <div className="featured-books">
      <header>
        <h2>Featured Books</h2>
      </header>
      <hr />

      <Slider
        className="featured-books-external-carousel"
        autoplay={true}
        autoplaySpeed={2000}
        // setting accessibility to false as the library isn't accessible
        accessibility={true}
        centerMode={true}
        dots={true}
        adaptiveHeight={false}
        onInit={updateVisibleCarouselItems}
        infinite={true}
        pauseOnHover={true}
        pauseOnFocus={true}
        slidesToShow={slidesPerFrame}
        onReInit={updateVisibleCarouselItems}
        prevArrow={
          <button>
	    <span className="visually-hidden">Previous Slide</span>
            <svg
              width="6"
              height="12"
              viewBox="0 0 6 12"
              fill="none"
              xmlns="http://www.w3.org/2000/svg">
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M5.5 11.0577L5.49846 0.67308L0.5 5.86539L5.5 11.0577Z"
                fill="black"
              />
            </svg>
          </button>
        }
        nextArrow={
          <button>
	    <span className="visually-hidden">Next Slide</span>
            <svg
              width="6"
              height="12"
              viewBox="0 0 6 12"
              fill="none"
              xmlns="http://www.w3.org/2000/svg">
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M0.5 11.0577L0.501541 0.67308L5.5 5.86539L0.5 11.0577Z"
                fill="black"
              />
            </svg>
          </button>
        }>
        {books.map((book) => (
          <FeaturedBookItem book={book} key={book.id} />
        ))}
      </Slider>
    </div>
  );
};

export default FeaturedBooksCarousel;
