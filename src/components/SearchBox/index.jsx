import React, { useContext, useRef } from 'react';
import { Context } from '../../App';

import { ReactComponent as SearchIcon } from '../../assets/search.svg';
import { ReactComponent as BackIcon } from '../../assets/arrow.svg';

import './SearchBox.scss';
import { useHistory } from 'react-router';

const SearchBox = ({ isSearchShown, setIsSearchShown }) => {
  const { searchText, setSearchText } = useContext(Context);
  const inputRef = useRef(null);

  const history = useHistory();

  return (
    <form
      className={`search-box${isSearchShown ? ' search-box--shown' : ''}`}
      onSubmit={(e) => {
        e.preventDefault();
        history.push('/search');
      }}>
      <button
        type="button"
        className="close-search"
        onClick={() => {
          setIsSearchShown(false);

          if (history.length > 1) history.goBack();
        }}>
        <span className="visually-hidden">Close</span>
        <BackIcon />
      </button>
      <label htmlFor="searchInput" className="visually-hidden">
        Search
      </label>
      <input
        ref={inputRef}
        onChange={(e) => {
          setSearchText(e.target.value);

          if (
            searchText.trim === '' &&
            history.length > 1 &&
            history.location.pathname.toLowerCase() === '/search'
          ) {
            history.go(-1);
          }
        }}
        value={searchText}
        placeholder="Search books, genres, authors, etc."
        type="text"
      />
      <button type="submit">
        <span className="visually-hidden">Submit</span>
        <SearchIcon />
      </button>
    </form>
  );
};

export default SearchBox;
