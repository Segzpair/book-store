import React, { useContext } from 'react';

import { Context } from '../../App';

import './TestDataIndicator.scss';

const TestDataIndicator = () => {
  const { isTestData } = useContext(Context);

  return isTestData ? (
    <div className="test-data-indicator">
      <small>
        Currently Using Test Data, Reload to Retry Fetching from Backend.
      </small>
    </div>
  ) : (
    <></>
  );
};

export default TestDataIndicator;
