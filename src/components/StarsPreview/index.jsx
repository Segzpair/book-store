import React from 'react';
import { ReactComponent as StarIcon } from '../../assets/star.svg';

const StarsPreview = ({ totalStars = 5, rating }) => {
  const placeholderArray = [];

  for (let i = 1; i <= totalStars; i++) {
    placeholderArray.push(i);
  }

  return (
    <div>
      {placeholderArray.map((num, index) => {
        if (Math.round(rating) >= num) {
          return (
            <StarIcon className="star-icon" fill={'#EBA430'} key={index} />
          );
        } else {
          return <StarIcon className="star-icon" fill={'#ddd'} key={index} />;
        }
      })}
    </div>
  );
};

export default StarsPreview;
