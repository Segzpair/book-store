import React, { createContext, useReducer, useState, useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { useQuery } from '@apollo/client';
import { cartReducer } from './reducers/reducers';
import { getAllBooksQuery } from './graphql/queries';

import Wrapper from './containers/Wrapper';
import Header from './components/Header';
import Cart from './components/Cart';
import Footer from './components/Footer';
import HomePage from './pages/HomePage';
import BookDetailsPage from './pages/BookDetailsPage';
import NotFound from './pages/NotFound';
import SearchPage from './pages/SearchPage';
import ErrorBoundary from './pages/ErrorBoundary';
import LoadingScreen from './components/LoadingScreen';
import TestDataIndicator from './components/TestDataIndicator';

import testData from './data/test-data';

export const Context = createContext(null);

function App() {
  const [isCartShown, setIsCartShown] = useState(false);
  const [cartState, dispatchCart] = useReducer(cartReducer, []);
  const [booksData, setBooksData] = useState([]);
  const [searchText, setSearchText] = useState('');
  const [isTestData, setIsTestData] = useState(false);

  const contextObject = {
    cartState,
    dispatchCart,
    booksData,
    setBooksData,
    searchText,
    setSearchText,
    isTestData,
    testData,
    setIsTestData,
  };

  // fetch data from API and update state
  const { loading: isLoading, error, data } = useQuery(getAllBooksQuery());

  useEffect(() => {
    if (!isLoading && !error && data && data.books) {
      setBooksData(data.books);
    }

    if ((!isLoading && error) || isTestData) {
      // fallback to test data if an error occurs or if using testData
      setBooksData(testData);
    }
  }, [data, error, isLoading, isTestData]);

  return (
    <Router>
      <Context.Provider value={contextObject}>
        <Header {...{ cartState, setIsCartShown }} />
        <Wrapper>
          {isLoading ? (
            <LoadingScreen />
          ) : error && !isTestData ? (
            <div className="not-found">
              <h2>Oops! :(</h2>
              <p>An error occured while fetching data.</p>
              <button
                className="btn-primary"
                onClick={() => setIsTestData(true)}>
                Click Here to use Test Data
              </button>
            </div>
          ) : (
            <ErrorBoundary>
              {isTestData ? <TestDataIndicator /> : <></>}
              <Cart
                {...{ isCartShown, setIsCartShown, cartState, dispatchCart }}
              />
              <Switch>
                <Route
                  path="/"
                  exact
                  render={() => <HomePage {...{ isLoading, booksData }} />}
                />
                <Route
                  path="/books"
                  exact
                  render={() => <HomePage {...{ isLoading, booksData }} />}
                />
                <Route path="/books/:bookId" component={BookDetailsPage} />
                <Route
                  path="/search"
                  exact
                  render={() => <SearchPage books={booksData} />}
                />
                <Route path="*" component={NotFound} />
              </Switch>
              <Footer />
            </ErrorBoundary>
          )}
        </Wrapper>
      </Context.Provider>
    </Router>
  );
}

export default App;
