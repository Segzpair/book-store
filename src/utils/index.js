export const formatMoney = (amount) =>
  Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
  }).format(amount);

export function addItemToCart(dispatchCart, currentBook) {
  dispatchCart({ type: 'ADD_ITEM', payload: { ...currentBook } });
}

export function checkIsBookInStock(cartState, currentBook) {
  // if available copies is less than 1, return false
  if (!currentBook || currentBook.available_copies < 1) return false;

  // find number of copies in cart
  const quantityInCart =
    findBookById(cartState, currentBook.id)?.quantityInCart || 0;

  if (quantityInCart < currentBook.available_copies) return true;

  return false;
}

export function findBookById(booksArr, bookId) {
  return booksArr.find((book) => +book.id === +bookId);
}

export function getCopiesLeftSentence(numberOfItems) {
  if (!numberOfItems) return 'Out of Stock';

  return numberOfItems > 1
    ? `${numberOfItems} Copies Available`
    : `${numberOfItems} Copy Available`;
}

export function getCopiesLeft(cartState, currentBook) {
  // find number of copies in cart
  const quantityInCart =
    findBookById(cartState, currentBook.id)?.quantityInCart || 0;

  return currentBook.available_copies - quantityInCart;
}

export function isBookFeatured(book) {
  return !!book.featured;
}

export const filterBySearchText = (book, searchText) => {
  let result = false;

  if (
    book.title &&
    book.title.toLowerCase().includes(searchText.toLowerCase().trim())
  ) {
    result = true;
  }

  if (
    book.authors &&
    book.authors.length &&
    book.authors
      .map((word) => word.name.toLowerCase())
      .join(' ')
      .includes(searchText.toLowerCase().trim())
  ) {
    result = true;
  }

  if (
    book.genres &&
    book.genres.length &&
    book.genres
      .map((word) => word.name.toLowerCase())
      .join(' ')
      .includes(searchText.toLowerCase().trim())
  ) {
    result = true;
  }

  return result;
};
