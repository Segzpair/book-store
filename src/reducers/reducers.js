export const cartReducer = (state, action) => {
  switch (action.type) {
    case 'ADD_ITEM':
      // check if item exists in cart

      if (state.some((cartItem) => +cartItem.id === +action.payload.id)) {
        return state.map((cartItem) => {
          // increase quantity if item is found and quantity in cart is less than available_copies

          if (
            +cartItem.id === +action.payload.id &&
            cartItem.quantityInCart < action.payload.available_copies
          ) {
            return {
              ...cartItem,
              quantityInCart: cartItem.quantityInCart + 1,
            };
          } else {
            return cartItem;
          }
        });
      } else {
        // add item to state array if available_copies > 0

        if (action.payload.available_copies > 0) {
          return [...state, { ...action.payload, quantityInCart: 1 }];
        } else return state;
      }

    case 'REMOVE_ITEM':
      // remove item with id from state array
      return state.filter((cartItem) => +cartItem.id !== +action.payload.id);

    case 'INCREMENT_ITEM':
      // increment item with id if there are available copies

      return state.map((cartItem) =>
        +cartItem.id === +action.payload.id &&
        cartItem.quantityInCart < action.payload.available_copies
          ? { ...cartItem, quantityInCart: cartItem.quantityInCart + 1 }
          : cartItem
      );

    case 'DECREMENT_ITEM': {
      // decrement item with id

      const index = state.findIndex(
        (cartItem) => +cartItem.id === +action.payload.id
      );

      // check if more than 1 item is in cart

      if (state[index].quantityInCart > 1) {
        return state.map((cartItem) => {
          if (+cartItem.id === +action.payload.id) {
            return {
              ...cartItem,
              quantityInCart: cartItem.quantityInCart - 1,
            };
          } else {
            return cartItem;
          }
        });
      } else {
        // remove item from cart if quantity becomes 0

        return state.filter((cartItem) => +cartItem.id !== +action.payload.id);
      }
    }

    default:
      return state;
  }
};
