import gql from 'graphql-tag';

export const getBookQuery = (id) => {
  return gql`
      query {
        book (id: ${id}) {
          id
          authors {
            name
            id
          }
          available_copies
          genres {
            name
            id
          }
          image_url
          likes
          number_of_purchases
          price
          publisher
          rating
          release_date
          subtitle
          tags { 
            name
            id
          }
          title
          full_description
        }
      }
    `;
};

export const getAllBooksQuery = () => {
  return gql`
    query {
      books {
        id
        created_at
        updated_at
        title
        subtitle
        publisher
        release_date
        number_of_purchases
        likes
        rating
        price
        currency
        available_copies
        full_description
        featured
        image_url
        published_at
        authors {
          id
          name
        }
        tags {
          id
          name
        }
        genres {
          id
          name
        }
      }
    }
  `;
};
