# React Book Store

## Setup Instructions

Once in the project directory, you can run:

### `npm install`

This installs all the app's dependencies.

### `npm start`

This runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm run build`

This builds the app for production to the `build` folder.

## Assumptions Made

I made the following assumptions while building the application

1. I disabled the Add-To-Cart buttons once the quantity in the cart was equal to the total number of copies available. This was only noticable on items that were originally in stock as out of stock items have no buttons.

2. The user needs to be aware of the loading state, so I added a loading screen when fetching data.

3. If the app fails to fetch data, I added test data to enable one preview the application still.

4. Added an indicator to show when test data is in use.